## 1.0.0

* feat: (BREAKING) change all outputs from list of single value to the value
* maintenance: (BREAKING) bump the minimal Terraform version to 1.3+
* chore: bump pre-commit hooks
* test: remove Jenkinsfile
* chore: bump pre-commit hooks

## 0.1.1

* maintenance: fix TF lint issues
* chore: bump pre-commit hooks
* test: adds gitlab ci

## 0.1.0

* feat: module init

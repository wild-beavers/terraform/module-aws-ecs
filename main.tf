locals {
  tags = { managed-by = "Terraform" }
}

#####
# IAM
#####

resource "aws_iam_role" "execution_role" {
  for_each = var.cluster_execution_role_enabled == true ? { 0 = 0 } : {}

  name               = format("%s%s", var.prefix, var.cluster_execution_role_name)
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
  tags = merge(
    var.tags,
    var.cluster_execution_role_tags,
    local.tags,
  )
}

resource "aws_iam_policy" "execution_policy" {
  for_each = var.cluster_execution_role_enabled == true ? { 0 = 0 } : {}

  name        = format("%s%s", var.prefix, var.cluster_execution_policy_name)
  description = "Provides access to other AWS service resources that are required to run Amazon ECS tasks"
  policy      = data.aws_iam_policy_document.execution_role_policy.json
  tags        = var.cluster_execution_policy_tags
}

resource "aws_iam_role_policy_attachment" "execution_role_to_ecs_task_execution_role" {
  for_each = var.cluster_execution_role_enabled == true ? { 0 = 0 } : {}

  role       = aws_iam_role.execution_role["0"].name
  policy_arn = aws_iam_policy.execution_policy["0"].arn
}

#####
# ECS
#####

resource "aws_ecs_cluster" "this" {
  for_each = var.cluster_enabled == true ? { 0 = 0 } : {}

  name = format("%s%s", var.prefix, var.cluster_name)

  dynamic "setting" {
    for_each = var.cluster_setting

    content {
      name  = setting.value["name"]
      value = setting.value["value"]
    }
  }

  dynamic "configuration" {
    for_each = var.cluster_configuration

    content {
      execute_command_configuration {
        kms_key_id = lookup(configuration.value, "kms_key_id", null)
        logging    = lookup(configuration.value, "logging", null)

        dynamic "log_configuration" {
          for_each = lookup(configuration.value, "use_log_configuration", false) ? [configuration.value["log_configuration"]] : []

          content {
            cloud_watch_encryption_enabled = lookup(log_configuration.value, "cloud_watch_encryption_enabled", null)
            cloud_watch_log_group_name     = lookup(log_configuration.value, "cloud_watch_log_group_name", null)
            s3_bucket_name                 = lookup(log_configuration.value, "s3_bucket_name", null)
            s3_bucket_encryption_enabled   = lookup(log_configuration.value, "s3_bucket_encryption_enabled", null)
            s3_key_prefix                  = lookup(log_configuration.value, "s3_key_prefix", null)
          }
        }
      }
    }
  }

  tags = merge(
    var.tags,
    var.cluster_tags,
    local.tags,
  )

}

resource "aws_ecs_cluster_capacity_providers" "this" {
  for_each = var.cluster_enabled == true ? { 0 = 0 } : {}

  cluster_name       = aws_ecs_cluster.this["0"].name
  capacity_providers = var.cluster_capacity_providers

  dynamic "default_capacity_provider_strategy" {
    for_each = var.cluster_default_capacity_provider_strategy

    content {
      capacity_provider = default_capacity_provider_strategy.value["capacity_provider"]
      weight            = lookup(default_capacity_provider_strategy.value, "weight", null)
      base              = lookup(default_capacity_provider_strategy.value, "base", null)
    }
  }
}

resource "aws_ecs_task_definition" "this" {
  for_each = var.task_definition_enabled == true ? { 0 = 0 } : {}

  container_definitions    = jsonencode(var.task_definition_container_definitions)
  family                   = var.task_definition_name != null ? format("%s%s", var.prefix, var.task_definition_name) : aws_ecs_cluster.this["0"].name
  cpu                      = var.task_definition_fargate_cpu
  memory                   = var.task_definition_fargate_mem
  execution_role_arn       = var.task_definition_use_external_execution_role ? var.task_definition_external_execution_role_arn : try(aws_iam_role.execution_role["0"].arn, "")
  network_mode             = var.task_definition_network_mode
  ipc_mode                 = var.task_definition_ipc_mode
  pid_mode                 = var.task_definition_pid_mode
  requires_compatibilities = var.task_definition_requires_compatibilities
  task_role_arn            = var.task_definition_use_external_task_role ? var.task_definition_external_task_role_arn : try(aws_iam_role.execution_role["0"].arn, "")
  tags = merge(
    var.tags,
    var.task_definition_tags,
    local.tags,
  )

  dynamic "inference_accelerator" {
    for_each = var.task_definition_inference_accelerator

    content {
      device_name = inference_accelerator.value["device_name"]
      device_type = inference_accelerator.value["device_type"]
    }
  }

  dynamic "runtime_platform" {
    for_each = var.task_definition_runtime_platform

    content {
      operating_system_family = lookup(runtime_platform.value, "operating_system_family")
      cpu_architecture        = lookup(runtime_platform.value, "cpu_architecture")
    }
  }

  dynamic "placement_constraints" {
    for_each = var.task_definition_placement_constraints

    content {
      type       = placement_constraints.value["type"]
      expression = lookup(placement_constraints.value, "expression")
    }
  }

  dynamic "proxy_configuration" {
    for_each = var.task_definition_proxy_configuration

    content {
      container_name = proxy_configuration.value["container_name"]
      properties     = proxy_configuration.value["properties"]
      type           = lookup(proxy_configuration.value, "type")
    }
  }

  dynamic "ephemeral_storage" {
    for_each = var.task_definition_ephemeral_storage

    content {
      size_in_gib = ephemeral_storage.value["size_in_gib"]
    }
  }

  dynamic "volume" {
    for_each = var.task_definition_volumes

    content {
      name      = volume.value["name"]
      host_path = lookup(volume.value, "host_path", null)

      dynamic "docker_volume_configuration" {
        for_each = lookup(volume.value, "use_docker_volume_configuration") ? [volume.value["docker_volume_configuration"]] : []

        content {
          autoprovision = lookup(docker_volume_configuration.value, "autoprovision", null)
          driver        = lookup(docker_volume_configuration.value, "driver", null)
          driver_opts   = lookup(docker_volume_configuration.value, "driver_opts", null)
          labels        = lookup(docker_volume_configuration.value, "labels", null)
          scope         = lookup(docker_volume_configuration.value, "scope", null)
        }
      }
      dynamic "efs_volume_configuration" {
        for_each = lookup(volume.value, "use_efs_volume_configuration") ? [volume.value["efs_volume_configuration"]] : []

        content {
          file_system_id          = efs_volume_configuration.value["file_system_id"]
          root_directory          = lookup(efs_volume_configuration.value, "root_directory", null)
          transit_encryption      = lookup(efs_volume_configuration.value, "transit_encryption", null)
          transit_encryption_port = lookup(efs_volume_configuration.value, "transit_encryption_port", null)

          dynamic "authorization_config" {
            for_each = lookup(efs_volume_configuration.value, "use_authorization_config") ? [efs_volume_configuration.value["authorization_config"]] : []

            content {
              access_point_id = lookup(authorization_config.value, "access_point_id", null)
              iam             = lookup(authorization_config.value, "iam", null)
            }
          }
        }
      }

      dynamic "fsx_windows_file_server_volume_configuration" {
        for_each = lookup(volume.value, "use_fsx_windows_file_server_volume_configuration") ? [volume.value["fsx_windows_file_server_volume_configuration"]] : []

        content {
          file_system_id = fsx_windows_file_server_volume_configuration.value["file_system_id"]
          root_directory = fsx_windows_file_server_volume_configuration.value["root_directory"]
          authorization_config {
            credentials_parameter = fsx_windows_file_server_volume_configuration.value["credentials_parameter"]
            domain                = fsx_windows_file_server_volume_configuration.value["domain"]
          }
        }
      }
    }
  }
}

resource "aws_ecs_service" "this" {
  for_each = var.task_definition_enabled ? { 0 = 0 } : {}

  name                               = format("%s%s", var.prefix, var.service_name)
  deployment_maximum_percent         = var.service_deployment_maximum_percent
  deployment_minimum_healthy_percent = var.service_deployment_minimum_healthy_percent
  desired_count                      = var.service_desired_count
  enable_ecs_managed_tags            = var.service_enable_ecs_managed_tags
  enable_execute_command             = var.service_enable_execute_command
  force_new_deployment               = var.service_force_new_deployment
  health_check_grace_period_seconds  = var.service_health_check_grace_period_seconds
  iam_role                           = var.service_iam_role
  launch_type                        = var.service_launch_type
  task_definition                    = aws_ecs_task_definition.this["0"].arn
  wait_for_steady_state              = var.service_wait_for_steady_state
  tags = merge(
    var.tags,
    var.service_tags,
    local.tags,
  )

  dynamic "capacity_provider_strategy" {
    for_each = var.service_capacity_provider_strategy

    content {
      base              = lookup(capacity_provider_strategy.value, "base")
      capacity_provider = capacity_provider_strategy.value["capacity_provider"]
      weight            = capacity_provider_strategy.value["weight"]
    }
  }

  cluster = aws_ecs_cluster.this["0"].arn

  dynamic "deployment_circuit_breaker" {
    for_each = var.service_deployment_circuit_breaker

    content {
      enable   = deployment_circuit_breaker.value["enable"]
      rollback = deployment_circuit_breaker.value["rollback"]
    }
  }

  dynamic "deployment_controller" {
    for_each = var.service_deployment_controller

    content {
      type = lookup(deployment_controller.value, "type")
    }
  }

  dynamic "load_balancer" {
    for_each = var.service_load_balancer

    content {
      target_group_arn = load_balancer.value["target_group_arn"]
      container_name   = load_balancer.value["container_name"]
      container_port   = load_balancer.value["container_port"]
    }
  }

  dynamic "network_configuration" {
    for_each = var.service_network_configuration

    content {
      subnets          = network_configuration.value["subnets"]
      security_groups  = lookup(network_configuration.value, "security_groups")
      assign_public_ip = lookup(network_configuration.value, "assign_public_ip")
    }
  }

  dynamic "ordered_placement_strategy" {
    for_each = var.service_ordered_placement_strategy

    content {
      type  = ordered_placement_strategy.value["type"]
      field = lookup(ordered_placement_strategy.value, "field")
    }
  }

  dynamic "placement_constraints" {
    for_each = var.service_placement_constraints

    content {
      type       = placement_constraints.value["type"]
      expression = lookup(placement_constraints.value, "expression")
    }
  }

  platform_version    = var.service_platform_version
  propagate_tags      = var.service_propagate_tags
  scheduling_strategy = var.service_scheduling_strategy

  dynamic "service_registries" {
    for_each = var.service_service_registries

    content {
      registry_arn   = service_registries.value["registry_arn"]
      port           = lookup(service_registries.value, "port")
      container_port = lookup(service_registries.value, "container_port")
      container_name = lookup(service_registries.value, "container_name")
    }
  }
}

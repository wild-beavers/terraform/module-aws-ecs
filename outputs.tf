#####
# IAM
#####

output "task_definition_iam_execution_role_arn" {
  value = var.cluster_execution_role_enabled == true ? try(aws_iam_role.execution_role["0"].arn, "") : null
}

#####
# Cluster
#####

output "cluster_arn" {
  value = var.cluster_enabled ? try(aws_ecs_cluster.this["0"].arn, "") : null
}

output "cluster_id" {
  value = var.cluster_enabled ? try(aws_ecs_cluster.this["0"].id, "") : null
}

output "cluster_name" {
  value = var.cluster_enabled ? try(aws_ecs_cluster.this["0"].name, "") : null
}

#####
# Task definition
#####

output "task_definition_arn" {
  value = var.task_definition_enabled ? try(aws_ecs_task_definition.this["0"].arn, "") : null
}

output "task_definition_revision" {
  value = var.task_definition_enabled ? try(aws_ecs_task_definition.this["0"].revision, "") : null
}

output "task_definition_container_definition_json" {
  value = var.task_definition_enabled ? try(aws_ecs_task_definition.this["0"].container_definitions, "") : null
}

#####
# Service
#####

output "service_id" {
  value = var.task_definition_enabled ? try(aws_ecs_service.this["0"].id, "") : null
}

output "service_name" {
  value = var.task_definition_enabled ? try(aws_ecs_service.this["0"].name, "") : null
}

output "service_cluster" {
  value = var.task_definition_enabled ? try(aws_ecs_service.this["0"].cluster, "") : null
}

output "service_desired_count" {
  value = var.task_definition_enabled ? try(aws_ecs_service.this["0"].desired_count, "") : null
}

output "service_iam_role_arn" {
  value = var.task_definition_enabled ? try(aws_ecs_service.this["0"].iam_role, "") : null
}

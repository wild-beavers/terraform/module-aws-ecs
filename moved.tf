# to be removed after 1+

moved {
  from = aws_iam_role.execution_role[0]
  to   = aws_iam_role.execution_role["0"]
}

moved {
  from = aws_iam_policy.execution_policy[0]
  to   = aws_iam_policy.execution_policy["0"]
}

moved {
  from = aws_iam_role_policy_attachment.execution_role_to_ecs_task_execution_role[0]
  to   = aws_iam_role_policy_attachment.execution_role_to_ecs_task_execution_role["0"]
}

moved {
  from = aws_ecs_cluster.this[0]
  to   = aws_ecs_cluster.this["0"]
}

moved {
  from = aws_ecs_cluster_capacity_providers.this[0]
  to   = aws_ecs_cluster_capacity_providers.this["0"]
}

moved {
  from = aws_ecs_task_definition.this[0]
  to   = aws_ecs_task_definition.this["0"]
}

moved {
  from = aws_ecs_service.this[0]
  to   = aws_ecs_service.this["0"]
}

# to be removed after 1+

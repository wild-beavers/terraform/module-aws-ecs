#####
# Cluster
#####

output "cluster_arn" {
  value = module.example.cluster_arn
}

output "cluster_id" {
  value = module.example.cluster_id
}

output "cluster_name" {
  value = module.example.cluster_name
}

#####
# Task definition
#####

output "task_definition_arn" {
  value = module.example.task_definition_arn
}

output "task_definition_revision" {
  value = module.example.task_definition_revision
}
